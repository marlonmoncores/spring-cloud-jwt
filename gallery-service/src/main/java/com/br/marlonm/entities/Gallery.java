package com.br.marlonm.entities;

import lombok.Data;

import java.util.List;

@Data
public class Gallery {
    private int id;
    List<Object> images;
}
